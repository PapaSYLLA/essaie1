*** Settings ***
Library    RPA.Desktop
Library    Config
#Library    RPA.Browser.Selenium
Library    RPA.Windows
Library    Collections
Library    RequestsLibrary
Library    DateTime


*** Test Cases ***
Open Calculator and Perform Calculation
    ${log_file}    Set Variable    logs.txt
    ${current_time}    Get Current Date    result_format=%H:%M:%S
    ${dateFin}    Get Current Date    result_format=%H:%M:%S
    ${dateDebut}    Get Current Date    result_format=%H:%M:%S
    Create File    ${log_file}
    Append To File    ${log_file}    ${current_time} ****** Ouverture de l'application Calculatrice\n
    Open Application    calc.exe
    Sleep    5s
    Send Keys    id:CalculatorResults    1123456
    Append To File    ${log_file}    ${current_time} ******la première valeur remplit\n
    #Sleep    2s
    Send Keys    id:CalculatorResults    +
    Append To File    ${log_file}    ${current_time} ******l'opérateur remplit\n
    #Sleep    2s
    Send Keys    id:CalculatorResults    9
    Append To File    ${log_file}    ${current_time} ******la deuxième valeur remplit\n
    #Sleep    2s
    Send Keys    id:CalculatorResults    =
    Append To File    ${log_file}    ${current_time} ******l'opérateur remplit\n
    ${element}    Get Element    id:CalculatorResults
    Log To Console   Result: ${element.name}
    ${result}    Set Variable    ${element.name}
    Append To File    ${log_file}    ${current_time} ******Résultat: ${result}\n
    #Close Application
    Append To File    ${log_file}    ${current_time} ******Fermeture de l'application Calculatrice\n
    Log To Console   différence: ${dateFin} - ${dateDebut} 
    Append To File    ${log_file}     le temps d'éxécution du process est :${dateFin} - ${dateDebut} \n
    #Wait Until Element Is Visible    locator=Name:Seven
    #Wait Until Element Is Visible    locator=Name:Sept
    #Click    id:CalculatorResults    11
    #Click Element    id:CalculatorResults    11
    #Click Element    name:2
    #Click Element    name:equals
